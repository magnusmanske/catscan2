<?php

header('Content-type: text/html; charset=utf-8');
print '<meta http-equiv="refresh" content="0; url=./notice.html" />' ;
exit ( 0 ) ;

$botmode = isset ( $_REQUEST['botmode'] ) ;

include_once ( "php/common.php") ;

$language = trim ( strtolower ( get_request ( 'language' , 'de' ) ) ) ;
$project = trim ( strtolower ( get_request ( 'project' , 'wikipedia' ) ) ) ;
$depth = get_request ( 'depth' , 3 ) ;
$category = get_request ( 'category' , '' ) ;

if ( $botmode ) {
	header('Content-type: text/text; charset=utf-8');
} else {
	# Header
	print get_common_header ( "pages_in_cats.php" , "Pages in Category" ) ;
	print "<form method='get' taget='pages_in_cats.php' class='form form-inline'>
	<table class='table table-condensed table-striped'>
	<tr><th>Project</th><td><input type='text' name='project' value='{$project}'/></td></tr>
	<tr><th>Language</th><td><input type='text' name='language' value='{$language}'/></td></tr>
	<tr><th>Category</th><td><input type='text' name='category' value='{$category}'/></td></tr>
	<tr><th>Depth</th><td><input type='text' name='depth' value='{$depth}'/></td></tr>
	<tr><th></th><td><label><input type='checkbox' name='botmode' value='1'/> Bot-mode</label></td></tr>
	<tr><th></th><td><input type='submit' name='doit' value='Run'/></td></tr>
	</table>
	</form>" ;
}



if ( isset ( $_REQUEST['doit'] ) ) {
	$db = openDB ( $language , $project ) ;
	$pages = getPagesInCategory ( $db , $category , $depth ) ;
	if ( !$botmode ) print "Number of articles in {$category} ({$depth} deep) : " ;
	print count ( $pages ) ;
}


if ( !botmode ) print "</body></html>" ;

?>