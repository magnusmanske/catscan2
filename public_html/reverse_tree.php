<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR); // E_ALL|
ini_set('display_errors', 'On');

require_once ( 'php/common.php' ) ;

function fixQuotes ( $s ) {
	return htmlspecialchars($s) ;
}

function niceTitle ( $s ) {
	return fixQuotes ( str_replace ( '_' , ' ' , $s ) ) ;
}

function getCatLink ( $s , $title ) {
	global $language , $project ;
	return " [<a title='Click here to get to the Category page' href='//$language.$project.org/wiki/Category:" . fixQuotes($s) . "' target='_blank'>W</a>]" ;
}

function listReverseTree ( $title ) {
	global $ns , $language , $project , $title ;
	$j = array() ;
	
	$url = "?doit=1&language=" . urlencode($language) ;
	$url .= "&project=" . urlencode($project) ;
	$url .= "&title=" . urlencode($title) ;
	$url .= "&namespace=" . urlencode($ns) ;
	
	print "<p><a href='$url'>Permalink</a> | Jump to: <a href='#__stats'>Stats</a> | <a href='#____'>Category list</a></p><hr/>" ;
	print "<h2>Category reverse tree</h2>" ;
	$ok = getReverseTree ( array($title) , $j , $ns , true ) ;
	while ( count($ok) > 0 ) {
		$level++ ;
//		print "Level $level: " . implode ( ' | ' , $ok ) . "\n" ;
		$ok = getReverseTree ( $ok , $j , 14 , false ) ;
	}
	print "<hr/><p><a name='__stats'></a>Maximum depth: $level levels</p>" ;
	print "<p>Total categories along the way : " . count($j) . "</p>" ;
	
	print "<h2><a name='____'></a>List of all categories in reverse tree</h2>" ;
	print "<table class='table table-consensed table-striped'>" ;
	print "<tbody>" ;
	foreach ( $j AS $cat => $dummy ) {
		print "<tr><td><a title='Click here to get to the Category page' href='//$language.$project.org/wiki/Category:" . fixQuotes($cat) . "' target='_blank'>" . niceTitle($cat) . "</a></td></tr>" ;
	}
	print "</tbody></table>" ;
	
}

function getReverseTree ( $titles , &$j , $ns , $first ) {
	global $db , $language , $project ;
	$do = array() ;
	if ( count($titles) == 0 ) return $do ; // Nothing to do
	$t = array() ;
	foreach ( $titles AS $title ) $t[] = get_db_safe ( $title ) ;
	$sql = "SELECT cl_to,page_title FROM page,categorylinks WHERE page_title IN ('" . implode("','",$t) . "') AND page_namespace=$ns AND page_id=cl_from" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	while($o = $result->fetch_object()){
		if ( isset ( $j[$o->cl_to] ) ) continue ;
		print "<div>" ;
		if ( $first ) print niceTitle($o->page_title) ;
		else print "<a title='Click here to get to the first occurrence of this category in the tree' href='#14." . fixQuotes($o->page_title) . "'>" . niceTitle($o->page_title) . "</a>" . getCatLink($o->page_title) ;
		print " is in category <a name='14." . fixQuotes($o->cl_to) . "'></a><i>" . niceTitle($o->cl_to) . "</i>" . getCatLink($o->cl_to) . "</div>\n" ;
		$j[$o->cl_to] = 1 ;
		$do[] = $o->cl_to ;
	}
	return $do ;
}

$language = get_request ( 'language' , 'en' ) ;
$project = get_request ( 'project' , 'wikipedia' ) ;
$ns = get_request ( 'namespace' , '0' ) * 1 ;
$title = get_request ( 'title' , '' ) ;
$doit = isset ( $_REQUEST['doit'] ) ;
$db = '' ;

print get_common_header ( '' , 'Reverse category tree' ) ;

print "<div class='lead'>This tool can tell you all the categories your page or file is in, all the way down to the turtles...</div>
<div>
<form method='post' class='form form-inline inline-form'>
<table class='table'><tbody>
<tr><th>Wiki</th><td><input type='text' name='language' value='$language' /> . <input name='project' value='$project' type='text' /></td></tr>
<tr><th>Title</th><td><input type='text' name='title' value='$title' />, namespace # <input name='namespace' value='$ns' type='number' /> (0=main; 6=image)</td></tr>
<tr><td></td><td><input class='btn btn-primary' type='submit' name='doit' value='Do it!' /></td></tr>
</tbody></table>
</form>
</div>" ;

if ( $doit ) {
	$db = openDB ( $language , $project ) ;
	if ( $title != '' ) listReverseTree ( $title ) ;
}

print get_common_footer() ;

?>